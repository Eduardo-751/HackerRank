<a href="https://www.hackerrank.com/dashboard"><p align="center"><img src="https://hrcdn.net/fcore/assets/brand/logo-new-white-green-a5cb16e0ae.svg" height="50"/></p></a>

<br/>

## HackerRank Solutions

This repository contains my solutions to various coding challenges and problems from HackerRank. Each problem is solved using different programming languages, including C, C# and Java.

## Problems Solved

| Category                                                               | Documented | Total problems |
| ---------------------------------------------------------------------- | :--------: | :------------: |
| [Java Preparation ](./Java%20Preparation/README.md)                    |     9      |       63       |
| [C Preparation ](./C%20Preparation/README.md)                          |     17     |       25       |
| [3 Months Preparation Kit](./3%20Months%20Preparation%20Kit/README.md) |     8      |      104       |
| [Problem Solving](./Problem%20Solving/README.md)                       |     1      |      352       |
| Total                                                                  |     34     |      544       |

<br/>

## About HackerRank

[HackerRank](https://www.hackerrank.com/dashboard) is a platform that provides coding challenges and contests to help developers enhance their coding skills. This repository serves as a collection of my solutions to these challenges. The solutions are organized by problem category and programming language for easy navigation and reference.

#### HackerRank Profile:

[Eduardo Camargo](https://www.hackerrank.com/edu_s_camargo97)

<br/>

## HackerRank Badges

![C](/Certificate%20and%20Badges/Icons/C_Badges_Icon.PNG)
![Java](/Certificate%20and%20Badges/Icons/Java_Badges_Icon.PNG)

## HackerRank Certificates

<a href="/Certificate%20and%20Badges/Java_Basic_Certificate.PNG">
    <img src="/Certificate%20and%20Badges/Icons/Java_Certificate_Icon.PNG" alt="Java (Basic) Certificate"/>
</a>
